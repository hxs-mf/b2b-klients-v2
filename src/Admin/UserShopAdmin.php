<?php

namespace App\Admin;

use App\Entity\UserShop;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

final class UserShopAdmin extends AbstractAdmin
{
    public function configure()
    {
        parent::configure();
        $this->classnameLabel = "Użytkownicy";
    }

    public function toString($object)
    {
        return $object instanceof UserShop
            ? $object->getUsername()
            : 'Użytkownik'; // shown in the breadcrumb on the create view
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('username', TextType::class, [
            'label' => 'Imię i nazwisko'
        ])
            ->add('password', TextType::class, [
                'label' => 'Hasło'
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email'
            ])
            ->add('isActive', null, [
                'label' => 'Aktywny'
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('username');
        $datagridMapper->add('email');
        $datagridMapper->add('isActive');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('username', null, ['label' => 'Imię i nazwisko']);
        $listMapper->addIdentifier('email', null, ['Email']);
        $listMapper->addIdentifier('isActive', null, ['label' => 'Aktywny']);
    }
}
