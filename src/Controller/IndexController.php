<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserShop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {

        // $entityManager = $this->getDoctrine()->getManager();
        // $user = $entityManager->getRepository(UserShop::class)->findAll();
        // dd($user);

        //  $entityManager = $this->getDoctrine()->getManager();
        // $user = $entityManager->getRepository(User::class)->findAll();
        // dd($user);

        if ($this->getUser()) {
            return $this->redirectToRoute('sonata_admin_redirect');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('index/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }
}
